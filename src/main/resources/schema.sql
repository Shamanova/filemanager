 DROP TABLE IF EXISTS files, notes, notes_version;

CREATE TABLE  files
  (
      id   INT AUTO_INCREMENT PRIMARY KEY,
      path VARCHAR(250) NOT NULL UNIQUE

  );

CREATE TABLE  notes
(
    id   INT AUTO_INCREMENT PRIMARY KEY,
    note VARCHAR(250) NOT NULL,
    date_note TIMESTAMP,
    files_id INT,
    foreign key (files_id) references files ON DELETE CASCADE
);

CREATE TABLE notes_version
(
    id   INT AUTO_INCREMENT PRIMARY KEY,
    note VARCHAR(250) NOT NULL,
    date_version TIMESTAMP,
    note_id INT,
    foreign key (note_id) references notes ON DELETE CASCADE
);