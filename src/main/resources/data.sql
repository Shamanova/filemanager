INSERT INTO files (path) VALUES
  ('123.pdf'),
  ('Folder1\123.pdf');


INSERT INTO notes (note, date_note, files_id) VALUES
( 'note 1 123.pdf', '2019-09-30 12:00:00.0', 1),
( 'note 2 123.pdf', '2019-09-30 13:00:00.0', 1),
( 'note 1 Folder1\123.pdf', '2019-09-30 14:00:00.0', 2),
( 'note 2 Folder1\123.pdf', '2019-09-30 15:00:00.0', 2);

INSERT INTO notes_version (note, date_version, note_id) VALUES
( 'note 1 123.pdf version 1', '2019-09-30 12:00:00.0', 1),
( 'note 1 123.pdf version 2', '2019-09-30 18:47:52.69', 1),
( 'note 2 123.pdf version 1', '2019-09-30 13:00:00.0', 2),
( 'note 2 123.pdf version 2', '2019-09-30 18:47:52.69', 2),
( 'note 1 Folder1\123.pdf version 1', '2019-09-30 14:00:00.0', 3),
( 'note 1 Folder1\123.pdf version 2', '2019-09-30 18:47:52.69', 3),
( 'note 1 Folder1\123.pdf version 3', '2019-09-30 18:47:52.69', 3),
( 'note 2 Folder1\123.pdf version 1', '2019-09-30 15:00:00.0', 4);