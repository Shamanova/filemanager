package com.shamanova.filemanager.repository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Note {

    private LocalDateTime date;
    private String text;
    private int id;

    public Note(LocalDateTime date, String text, int id) {
        this.date = date;
        this.text = text;
        this.id = id;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm ");
        return formatter.format(date) + ": " + text;
    }

    public int getId() {
        return id;
    }
}
