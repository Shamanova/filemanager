package com.shamanova.filemanager.repository;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
public class DBAccessorNotes implements AccessorNotes {
    private final JdbcTemplate jdbcTemplate;
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;


    public DBAccessorNotes(JdbcTemplate jdbcTemplate, NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Note> getFileNote(String fileName) {
        Object[] param = new Object[1];
        param[0] = fileName;
        return new ArrayList<>(jdbcTemplate.query("SELECT NOTES.id, NOTE, DATE_NOTE FROM NOTES, FILES WHERE NOTES.FILES_ID=FILES.ID AND FILES.PATH=?", param,
                (rs, rowNum) -> new Note(LocalDateTime.parse(rs.getString("DATE_NOTE").replace(" ", "T")),
                        rs.getString("note"), rs.getInt("id"))));
    }

    @Override
    public void addFileNote(String fileName, String noteText) {

        List<Integer> idsFiles = getIdFIle(fileName);
        if (idsFiles.isEmpty()) {
            jdbcTemplate.update("INSERT INTO files(path) VALUES ( ? )", fileName);
            idsFiles = getIdFIle(fileName);
        }

        int fileId = idsFiles.get(0);
        Object[] noteFile = new Object[3];
        noteFile[0] = noteText;
        noteFile[1] = fileId;
        noteFile[2] = LocalDateTime.now();

        jdbcTemplate.update("INSERT INTO notes(note, files_id, date_note) VALUES (?,?,?)", noteFile);

        int noteId = getIdNote(noteText, fileId, (LocalDateTime) noteFile[2]);
        addNoteVersion(noteId,noteText);
    }
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
     int getIdNote(String note, int fileId, LocalDateTime noteDate){

        SqlParameterSource namedParameters = new MapSqlParameterSource().addValue("noteText", note)
                .addValue("dateNote", Timestamp.valueOf(noteDate))
                .addValue("fileId", fileId);
        return namedParameterJdbcTemplate.queryForObject(
                "SELECT id FROM notes where note = :noteText and date_note = :dateNote and  files_id = :fileId", namedParameters, Integer.class);

    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Note> getNoteVersions(int noteId) {
        Object[] param = new Object[1];
        param[0] = noteId;
        return new ArrayList<>(jdbcTemplate.query("SELECT id, note, date_version  FROM notes_version WHERE notes_version.note_id=? ORDER BY date_version", param,
                (rs, rowNum) -> new Note(LocalDateTime.parse(rs.getString("date_version").replace(" ", "T"))
                        , rs.getString("note"), rs.getInt("id"))
        ));
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Note getFileNote(int noteId) {
        Object[] param = new Object[1];
        param[0] = noteId;
        ArrayList<Note> notes = new ArrayList<>(jdbcTemplate.query("SELECT id, NOTE, DATE_NOTE FROM NOTES WHERE NOTES.ID=?", param,
                (rs, rowNum) -> new Note(LocalDateTime.parse(rs.getString("DATE_NOTE").replace(" ", "T")),
                        rs.getString("note"), rs.getInt("id"))));
        return notes.get(0);
    }

    @Override
    public void addNoteVersion(int idNote, String text) {

        Object[] noteFile = new Object[3];
        noteFile[0] = text;
        noteFile[1] = LocalDateTime.now();
        noteFile[2] = idNote;

        jdbcTemplate.update("INSERT INTO notes_version(note, date_version, note_id) VALUES (?,?,?)", noteFile);
    }

    @Override
    public void updateNoteString(int idNote, String text) {

        Object[] noteFile = new Object[3];
        noteFile[0] = text;
        noteFile[1] = LocalDateTime.now();
        noteFile[2] = idNote;

        jdbcTemplate.update("UPDATE notes SET note = ?, date_note = ? WHERE id = ?", noteFile);
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<String> getAllFiles() {
        return new ArrayList<>(jdbcTemplate.query("SELECT path FROM files", (rs, rowNum) -> rs.getString("path")));
    }

    @Override
    public void deleteFilesLines(String filePath) {
        jdbcTemplate.update("DELETE FROM files WHERE files.path=?", filePath);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Integer> getIdFIle(String fileName) {
        Object[] param = new Object[1];
        param[0] = fileName;
        return new ArrayList<>(jdbcTemplate.query("SELECT ID FROM FILES WHERE FILES.PATH=?", param,
                (rs, rowNum) -> rs.getInt("id")));
    }
}
