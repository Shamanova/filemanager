package com.shamanova.filemanager.repository;

import com.shamanova.filemanager.service.FileService;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
public class DeletedFilesResolver {
    private final AccessorNotes accessorNotes;
    private final FileService fileService;

    public DeletedFilesResolver(AccessorNotes accessorNotes, FileService fileService) {
        this.accessorNotes = accessorNotes;
        this.fileService = fileService;
    }

    @PostConstruct
    public void deleteRedundantStrings() {
        List<String> filePaths = accessorNotes.getAllFiles();
        for (String filePath : filePaths) {
            if (!fileService.isFileExist(filePath)) {
                accessorNotes.deleteFilesLines(filePath);
            }
        }
    }

}
