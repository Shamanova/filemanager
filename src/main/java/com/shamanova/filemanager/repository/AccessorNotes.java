package com.shamanova.filemanager.repository;

import com.shamanova.filemanager.controllers.EditNoteData;

import java.nio.file.Path;
import java.util.List;

public interface AccessorNotes {
    List<Note> getFileNote(String fileName);

    void addFileNote(String fileName, String noteText);

    List<Note> getNoteVersions(int noteId);

    Note getFileNote(int noteId);

    void addNoteVersion(int idNote, String text);

    void updateNoteString(int idNote, String text);

    List<String> getAllFiles();

    void deleteFilesLines(String filePath);
}
