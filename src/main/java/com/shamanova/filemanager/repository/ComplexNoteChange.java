package com.shamanova.filemanager.repository;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ComplexNoteChange {
    private final AccessorNotes accessorNotes;

    public ComplexNoteChange(AccessorNotes accessorNotes) {
        this.accessorNotes = accessorNotes;
    }

    @Transactional(propagation = Propagation.SUPPORTS)
    public void ChangeNoteAndAddVersions(int idNote, String text) {
        accessorNotes.addNoteVersion(idNote, text);
        accessorNotes.updateNoteString(idNote, text);
    }
}
