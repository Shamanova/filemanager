package com.shamanova.filemanager.exeptions;

public abstract class FileException extends FileManagerException {
    private String fileName;
    private  String exceptionKey;



    public FileException(Throwable e, String fileName) {
        super(e);
        this.fileName = fileName;
    }

    public abstract String getExceptionKey();

    public final String getFileName() {
        return fileName;
    }
}
