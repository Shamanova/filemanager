package com.shamanova.filemanager.exeptions;

import java.io.IOException;

public class FileReadException extends FileException {
    private String exceptionKey = "fileReadException";
    public FileReadException(IOException e, String fileName) {
    super(e,fileName);
    }

    @Override
    public String getExceptionKey() {
        return exceptionKey;
    }
}
