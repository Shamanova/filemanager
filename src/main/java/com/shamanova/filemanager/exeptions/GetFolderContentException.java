package com.shamanova.filemanager.exeptions;

import java.io.IOException;
import java.nio.file.Path;

public class GetFolderContentException extends FileException {
    private String exceptionKey = "getFolderContentException";
//    private String fileName;

//    public GetFolderContentException() {
//        super();
//    }
//
//    public GetFolderContentException(String message) {
//        super(message);
//    }
//
//    public GetFolderContentException(String message, Throwable cause) {
//        super(message, cause);
//    }
//
//    public GetFolderContentException(Throwable cause) {
//        super(cause);
//    }

    public GetFolderContentException(IOException e, String fileName) {
        super(e,fileName);
//        this.fileName = fileName;

    }

    @Override
    public String getExceptionKey() {
        return this.exceptionKey;
    }
}
