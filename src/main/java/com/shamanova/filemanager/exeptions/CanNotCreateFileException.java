package com.shamanova.filemanager.exeptions;

import java.io.IOException;

public class CanNotCreateFileException extends FileException {
    private String exceptionKey = "fileCreateException";

    public CanNotCreateFileException(IOException e, String fileName) {
        super(e,fileName);
    }

    @Override
    public String getExceptionKey() {
        return exceptionKey;
    }
}
