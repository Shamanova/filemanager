package com.shamanova.filemanager.exeptions;

import java.io.IOException;

public class UploadFileException extends FileException {
    private String exceptionKey = "fileUploadException";
//    private String fileName;

//    public UploadFileException() {
//        super();
//    }
//
//    public UploadFileException(String message) {
//        super(message);
//    }
//
//    public UploadFileException(String message, Throwable cause) {
//        super(message, cause);
//    }
//
//    public UploadFileException(Throwable cause) {
//        super(cause);
//    }

    public UploadFileException(IOException e, String name) {
        super(e, name);
//        this.fileName = name;
    }

    @Override
    public String getExceptionKey() {
        return exceptionKey;
    }
}
