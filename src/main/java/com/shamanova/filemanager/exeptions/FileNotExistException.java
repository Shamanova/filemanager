package com.shamanova.filemanager.exeptions;

import java.io.FileNotFoundException;

public class FileNotExistException extends FileException {
    private String exceptionKey = "fileUNotExistException";

    public FileNotExistException(FileNotFoundException e, String fileName) {
    super(e,fileName);
    }

    @Override
    public String getExceptionKey() {
        return null;
    }
}
