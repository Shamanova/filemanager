package com.shamanova.filemanager.exeptions;

import java.io.IOException;

public class FileSaveException extends FileException {
    private String exceptionKey = "FileSaveException";

    public FileSaveException(IOException e, String fileName) {
        super(e, fileName);
    }

    @Override
    public String getExceptionKey() {
        return exceptionKey;
    }
}
