package com.shamanova.filemanager.exeptions;

import java.io.IOException;
import java.nio.file.Path;

public class FileAlreadyExistsException extends FileException {
    private String exceptionKey = "fileAlreadyExists";
//    private String fileName;

//    public FileAlreadyExistsException() {
//        super();
//    }
//
//    public FileAlreadyExistsException(String message) {
//        super(message);
//    }
//
//    public FileAlreadyExistsException(String message, Throwable cause) {
//        super(message, cause);
//    }
//
//    public FileAlreadyExistsException(Throwable cause) {
//        super(cause);
//    }

    public FileAlreadyExistsException(Throwable e, String fileName) {
        super(e,fileName);
//        this.fileName = fileName;
    }

//    public String getFileName() {
//        return fileName;
//    }
//
    @Override
    public String getExceptionKey() {
        return exceptionKey;
    }
}
