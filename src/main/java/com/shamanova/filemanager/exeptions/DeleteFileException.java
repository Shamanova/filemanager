package com.shamanova.filemanager.exeptions;

import java.io.IOException;

public class DeleteFileException extends FileException {
    private String exceptionKey = "fileDeleteException";

    public DeleteFileException(IOException e, String s) {
        super(e, s);
    }

    @Override
    public String getExceptionKey() {
        return exceptionKey;
    }
}
