package com.shamanova.filemanager.service;

import com.shamanova.filemanager.exeptions.*;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.DosFileAttributeView;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;

@Service
public class FileService {
    private final AppProperties properties;
    private final IconExtensionMapping iconExtensionMapping;

    public FileService(AppProperties properties, IconExtensionMapping iconExtensionMapping) {
        this.properties = properties;
        this.iconExtensionMapping = iconExtensionMapping;
    }

    public List<FsResource> rootContent() {
        Path rootPath = properties.getRootPath();
        return resolveContent(rootPath);
    }

    public List<FsResource> relativeContent(String relativePath) {
        Path resolved = resolvePath(relativePath);
        return resolveContent(resolved);
    }

    public Optional<Path> backWard(String path) {
        return Optional.ofNullable(Paths.get(path).getParent());
    }

    private List<FsResource> resolveContent(Path path) {
        try (Stream<Path> paths = Files.list(path)) {
            return paths.map(this::CreateFileResourse).collect(toList());
        } catch (IOException e) {
            throw new GetFolderContentException(e, path.getFileName().toString());
        }
    }

    private FsResource CreateFileResourse(Path pathValue){
        Path relativize = properties.getRootPath().relativize(pathValue);
        boolean isDirectory = Files.isDirectory(pathValue);
        Optional<String> extension = getExtensionByStringHandling(pathValue.toString());
        Boolean isTxt = extension.map(extention -> extention.equals("txt")).orElse(false);
        String iconPath = "";
        if (isDirectory) {
            iconPath = iconExtensionMapping.getIcon("folder");
        } else {
            iconPath = iconExtensionMapping.getIcon(extension.orElse("unknown"));
        }
        return new FsResource(relativize, isDirectory, isTxt, iconPath);
    }

    public void uploadFile(MultipartFile file, String currentPath) {
        String uploadFileName = requireNonNull(file.getOriginalFilename(), "Upload file name is null");
        try {
            file.transferTo(resolvePath(currentPath, uploadFileName));
        } catch (IOException e) {
            throw new UploadFileException(e, file.getName());
        }
    }

    public Resource loadFileAsResource(String fileName) {
        Path currentFile = resolvePath(fileName).normalize();
        return new FileSystemResource(currentFile);
    }

    public void createFolder(String folderName, String currentPath) {
        Path fullPath;
        if (currentPath == null || currentPath.equals("")) {
            fullPath = resolvePath(folderName);
        } else {
            fullPath = resolvePath(currentPath, folderName);
        }
        try {
            Files.createDirectory(fullPath);
        } catch (IOException e) {
            throw new FileAlreadyExistsException(e, fullPath.getFileName().toString());
        }
    }

    public void createFile(String fileName, String currentPath) {
        if (!fileName.contains(".")) {
            throw new IllegalArgumentException("File name must contains dot symbol");
        }
        Path fullPath = resolvePath(currentPath, fileName);
        try {
            Files.createFile(fullPath);

            DosFileAttributeView dosView = Files.getFileAttributeView(fullPath, DosFileAttributeView.class);
            dosView.setHidden(false);
            dosView.setReadOnly(false);

        } catch (IOException e) {
            throw new CanNotCreateFileException(e, fileName);
        }
    }

    public Optional<Path> deleteFile(String currentPath) {
        Path resolvePath = resolvePath(currentPath);
        if (Files.isDirectory(resolvePath)) {
            try (Stream<Path> paths = Files.walk(resolvePath)) {
                paths.sorted(Comparator.reverseOrder())
                        .map(Path::toFile)
                        .forEach(File::delete);
            } catch (IOException e) {
                throw new DeleteFileException(e, resolvePath.getFileName().toString());
            }
        } else {
            new File(resolvePath.toString()).delete();
        }
        return Optional.ofNullable(Paths.get(currentPath).getParent());
    }


    private Path resolvePath(String... paths) {
        return Arrays.stream(paths).map(Paths::get).reduce(properties.getRootPath(), Path::resolve);
    }

    public String getCharSet(String fileName) {
        try {
            return String.join("\r\n", Files.readAllLines(resolvePath(fileName), StandardCharsets.UTF_8));
        } catch (IOException e) {
            throw new FileReadException(e, fileName);
        }
    }

    public void saveFile(String fileName, String content) {
        try (FileWriter fileWriter = new FileWriter(resolvePath(fileName).toString());
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
            bufferedWriter.write(content);
        } catch (IOException e) {
            new FileSaveException(e,fileName);
        }
    }

    private Optional<String> getExtensionByStringHandling(String filename) {
        return Optional.ofNullable(filename)
                .filter(f -> f.contains("."))
                .map(f -> f.substring(f.lastIndexOf(".") + 1).toLowerCase());
    }

    public boolean isFileExist(String filePath) {
        return Files.exists(resolvePath(filePath));
    }
}
