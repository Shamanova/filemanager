package com.shamanova.filemanager.service;

import java.nio.file.Path;

public class FsResource {
    private final Path path;
    private final boolean isDirectory;
    private final boolean isTxt;
    private final String iconPath;

    public FsResource(Path path, boolean isDirectory, boolean isTxt, String iconPath) {
        this.path = path;
        this.isDirectory = isDirectory;
        this.isTxt = isTxt;
        this.iconPath = iconPath;
    }

    public boolean isDirectory() {
        return this.isDirectory;
    }

    public boolean isTxt(){
        return this.isTxt;
    }

    public String resourceName() {
        return path.getFileName().toString();
    }

    public String path() {
        return path.toString();
    }

    public String getIconPath() {
        return iconPath;
    }
}
