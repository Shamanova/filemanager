package com.shamanova.filemanager.service;

import com.shamanova.filemanager.repository.AccessorNotes;
import org.springframework.stereotype.Service;

import java.nio.file.Path;
import java.util.Optional;

@Service
public class ComplexDeleteFileService {
    private final FileService fileService;
    private final AccessorNotes accessorNotes;

    public ComplexDeleteFileService(FileService fileService, AccessorNotes accessorNotes) {
        this.fileService = fileService;
        this.accessorNotes = accessorNotes;
    }

    public Optional<Path> deleteFileAndDbNotes(String path){
        this.accessorNotes.deleteFilesLines(path);
        Optional<Path> resultPath = this.fileService.deleteFile(path);
        return resultPath;
    }
}
