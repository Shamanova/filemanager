package com.shamanova.filemanager.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

@Component
public class AppProperties {

    private String rootPath;
    private String iconExtensionPath;

    public AppProperties(@Value("${upload.path}") String rootPath,
                         @Value("${iconsExtension.path}") String iconExtensionPath) {
        this.rootPath = rootPath;
        this.iconExtensionPath = iconExtensionPath;
    }

    public Path getRootPath() {
        return Paths.get(this.rootPath);
    }

    public Resource getIconExtensionPath() {
        return new ClassPathResource(iconExtensionPath);
    }
}