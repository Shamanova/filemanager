package com.shamanova.filemanager.service;

import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.annotation.PostConstruct;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Service
public class IconExtensionMapping {
    private final AppProperties properties;
    private final Map<String, String> iconsExtensions = new HashMap<>();

    public IconExtensionMapping(AppProperties properties) {
        this.properties = properties;
    }

    @PostConstruct
    private void constructMap() {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(properties.getIconExtensionPath().getFile());
            NodeList properties = document.getDocumentElement().getElementsByTagName("property");
            for (int i = 0; i < properties.getLength(); i++) {
                Node item = properties.item(i);
                NamedNodeMap attributes = item.getAttributes();
                iconsExtensions.put(attributes.getNamedItem("extension").getNodeValue(), attributes.getNamedItem("icon").getNodeValue());
            }
        } catch (ParserConfigurationException | IOException | SAXException e) {
            e.printStackTrace();
        }
    }

    public String getIcon(String extension) {
        String iconPath = iconsExtensions.get(extension);
        return iconPath == null ? iconsExtensions.get("unknown") : iconPath;
    }
}
