package com.shamanova.filemanager.config;

import com.shamanova.filemanager.repository.DeletedFilesResolver;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableScheduling
public class ScheduleConfig {
    private final DeletedFilesResolver deletedFilesResolver;

    public ScheduleConfig(DeletedFilesResolver deletedFilesResolver) {
        this.deletedFilesResolver = deletedFilesResolver;
    }

    @Scheduled(fixedDelay = 10000)
    public void scheduleFixedDelayTask() {
        deletedFilesResolver.deleteRedundantStrings();
    }
}
