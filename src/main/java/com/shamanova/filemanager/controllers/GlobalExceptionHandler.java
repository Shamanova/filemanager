package com.shamanova.filemanager.controllers;

import com.shamanova.filemanager.exeptions.FileAlreadyExistsException;
import com.shamanova.filemanager.exeptions.FileException;
import com.shamanova.filemanager.exeptions.FileManagerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Locale;

@ControllerAdvice
public class GlobalExceptionHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);


    private final MessageSource messageSource;

    public GlobalExceptionHandler(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @ExceptionHandler({FileException.class})
    public String handleException(FileException exception, Model model, Locale locale) {
        LOGGER.error(exception.getMessage());

        model.addAttribute("errorText", messageSource.getMessage(exception.getExceptionKey(), new Object[]{exception.getFileName()},locale));
        return "error";
    }

    @ExceptionHandler({Exception.class})
    public String handleAllException(Exception exception, Model model) {
        LOGGER.error(exception.getMessage());

        model.addAttribute("errorText", exception.getMessage());
        return "error";
    }

}
