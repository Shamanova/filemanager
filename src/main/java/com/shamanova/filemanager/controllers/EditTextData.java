package com.shamanova.filemanager.controllers;

public class EditTextData {
    private String fileName;
    private String content;

    public EditTextData(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "EditTextData{" +
                "filePath='" + fileName + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
