package com.shamanova.filemanager.controllers;

import com.shamanova.filemanager.repository.AccessorNotes;
import com.shamanova.filemanager.service.ComplexDeleteFileService;
import com.shamanova.filemanager.service.FileService;
import com.shamanova.filemanager.service.FsResource;
import org.springframework.core.io.Resource;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import java.nio.charset.StandardCharsets;
import java.util.List;


@Controller
@RequestMapping("/")
public class FileManagerController {
    private final FileService fileService;
    private final AccessorNotes accessorNotes;
    private final ComplexDeleteFileService complexDeleteFileService;

    public FileManagerController(FileService fileService, AccessorNotes accessorNotes, ComplexDeleteFileService complexDeleteFileService) {
        this.fileService = fileService;
        this.accessorNotes = accessorNotes;
        this.complexDeleteFileService = complexDeleteFileService;
    }

    @GetMapping
    public String index(Model model) {
        List<FsResource> files = this.fileService.rootContent();
        model.addAttribute("files", files);
        return "index";
    }

    @GetMapping("/uploadFile")
    public String moveToUploadFile() {
        return "upload_file";
    }

    @PostMapping("/uploadFile")
    public String add(@RequestParam("file") MultipartFile file, @RequestParam("name") String currentPath, RedirectAttributes redirectAttributes) {
        if (file != null) {
            this.fileService.uploadFile(file, currentPath);
        }
        redirectAttributes.addAttribute("name", currentPath);
        if (currentPath.equals("")) {
            return "redirect:/";
        }
        return "redirect:/fileClick/";
    }


    @GetMapping("/fileClick")
    public String onClick(@RequestParam("name") String folderName, Model model) {
        List<FsResource> files = this.fileService.relativeContent(folderName);
        model.addAttribute("files", files);
        return "index";
    }

    @GetMapping("/backClick")
    public String backClick(@RequestParam("name") String folderName, RedirectAttributes redirectAttributes) {
        return this.fileService.backWard(folderName)
                .map(path -> {
                    redirectAttributes.addAttribute("name", path.toString());
                    return "redirect:/fileClick/";
                })
                .orElse("redirect:/");
    }

    @GetMapping("/downloadFile")
    public ResponseEntity<Resource> downloadFile(@RequestParam("name") String fileName) {
        Resource resource = fileService.loadFileAsResource(fileName);
        ContentDisposition contentDisposition = ContentDisposition.builder("attachment")
                .filename(resource.getFilename(), StandardCharsets.UTF_8)
                .build();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentDisposition(contentDisposition);
        httpHeaders.setContentType(MediaType.APPLICATION_OCTET_STREAM);

        return ResponseEntity.ok()
                .headers(httpHeaders)
                .body(resource);
    }


    @GetMapping("/createFolder")
    public String moveToCreateFolder(@RequestParam("name") String path, Model model) {
        model.addAttribute("createFolderData", new CreateFolderData(path));
        return "create_folder";
    }


    @PostMapping("/createFolder")
    public String createFolder(@ModelAttribute("createFolderData") CreateFolderData createFolderData,
                               RedirectAttributes redirectAttributes) {
        String folderName = createFolderData.getFolderName();
        String currentPath = createFolderData.getCurrentPath();
        this.fileService.createFolder(folderName, currentPath);
        if (currentPath == null || currentPath.equals("")) {
            return "redirect:/";
        } else {
            redirectAttributes.addAttribute("name", currentPath);
            return "redirect:/fileClick/";
        }
    }

    @GetMapping("/createFile")
    public String moveToCreateFile() {
        return "create_file";
    }

    @PostMapping("/createFile")
    public String createFile(@RequestParam("name") String currentPath,
                             @RequestParam("fileName") String fileName,
                             RedirectAttributes redirectAttributes) {
        this.fileService.createFile(fileName, currentPath);
        if (currentPath.equals("")) {
            return "redirect:/";
        }
        redirectAttributes.addAttribute("name", currentPath);
        return "redirect:/fileClick/";
    }

    @GetMapping("/delete") //todo @deletemapping
    public String deleteFile(@RequestParam("name") String currentPath,
                             RedirectAttributes redirectAttributes) {
        return this.complexDeleteFileService.deleteFileAndDbNotes(currentPath)
                .map(path -> {
                    redirectAttributes.addAttribute("name", path.toString());
                    return "redirect:/fileClick/";
                })
                .orElse("redirect:/");
    }
}
