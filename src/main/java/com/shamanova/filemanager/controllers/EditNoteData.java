package com.shamanova.filemanager.controllers;

public class EditNoteData {
    private String text;
    private int idNote;

    public EditNoteData(int idNote) {
        this.idNote = idNote;
    }

    public String getText() {
        return text;
    }

    public int getIdNote() {
        return idNote;
    }

    public void setText(String text) {
        this.text = text;
    }
}
