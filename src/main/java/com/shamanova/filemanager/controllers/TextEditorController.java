package com.shamanova.filemanager.controllers;

import com.shamanova.filemanager.service.FileService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/editText")
public class TextEditorController {

    private final FileService fileService;

    public TextEditorController(FileService fileService) {
        this.fileService = fileService;
    }

    @GetMapping
    public String toEdit(@RequestParam("name") String fileName,
                         Model model){
        String content = this.fileService.getCharSet(fileName);

        EditTextData editTextData = new EditTextData(fileName);
        editTextData.setContent(content);

        model.addAttribute("editTextData", editTextData);
        return "text_editor";
    }

    @PostMapping
    public String saveFile(@ModelAttribute("editTextData") EditTextData editTextData,
                           RedirectAttributes redirectAttributes){
        String fileName = editTextData.getFileName();
        String content = editTextData.getContent();
        this.fileService.saveFile(fileName,content);
        redirectAttributes.addAttribute("name",fileName);
        return "text_editor";
    }



}
