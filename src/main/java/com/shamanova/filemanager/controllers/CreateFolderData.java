package com.shamanova.filemanager.controllers;

public class CreateFolderData {
    private String folderName;
    private String currentPath;

    public CreateFolderData(String currentPath) {
        this.currentPath = currentPath;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    public String getFolderName() {
        return folderName;
    }

    public String getCurrentPath() {
        return currentPath;
    }

    @Override
    public String toString() {
        return "CreateFolderData{" +
                "folderName='" + folderName + '\'' +
                ", currentPath='" + currentPath + '\'' +
                '}';
    }
}
