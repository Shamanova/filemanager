package com.shamanova.filemanager.controllers;

import com.shamanova.filemanager.repository.AccessorNotes;
import com.shamanova.filemanager.repository.ComplexNoteChange;
import com.shamanova.filemanager.repository.Note;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
@RequestMapping("/notes")
public class NotesController {

    private final AccessorNotes accessorNotes;
    private final ComplexNoteChange complexNoteChange;

    public NotesController(AccessorNotes accessorNotes, ComplexNoteChange complexNoteChange) {
        this.accessorNotes = accessorNotes;
        this.complexNoteChange = complexNoteChange;
    }

    @GetMapping
    public String moveToNotes(@RequestParam("name") String fileName, Model model) {
        List<Note> notes = accessorNotes.getFileNote(fileName);
        model.addAttribute("notes", notes);
        return "notes";
    }

    @PostMapping
    public String createNote(@RequestParam("name") String fileName,
                             @RequestParam("noteText") String noteText,
                             RedirectAttributes redirectAttributes) {
        accessorNotes.addFileNote(fileName, noteText);
        redirectAttributes.addAttribute("name", fileName);
        return "redirect:/notes";
    }

    @GetMapping("/versions")
    public String showVersions(@RequestParam("noteId") int noteId,
                               Model model) {
        List<Note> noteVersions = accessorNotes.getNoteVersions(noteId);
        model.addAttribute("versions", noteVersions);
        return "versions";
    }

    @GetMapping("/edit")
    public String toEditNote(@RequestParam("noteId") int noteId,
                             Model model) {
        EditNoteData editNoteData = new EditNoteData(noteId);
        editNoteData.setText(accessorNotes.getFileNote(noteId).getText());
        model.addAttribute("editNoteData", editNoteData);
        return "edit_note";
    }

    @PostMapping("/edit")
    public String editNote(@ModelAttribute("editNoteData") EditNoteData editNoteData,
                         RedirectAttributes redirectAttributes){
        int idNote = editNoteData.getIdNote();
        String text = editNoteData.getText();
        complexNoteChange.ChangeNoteAndAddVersions(idNote,text);
        return "edit_note";
    }
}
