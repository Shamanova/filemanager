package com.shamanova.filemanager.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class InternalizationController {
    @GetMapping("/changeLang")
    public String getInternationalPage() {
        return "change_lang";
    }
}
